import {NgModule} from '@angular/core';
import {WorkspaceRoutingModule} from '../../routing/workspace-routing.module';
import { WorkspaceLayoutComponent } from './components/workspace-layout/workspace-layout.component';
import { WorkspaceToolbarComponent } from './components/workspace-toolbar/workspace-toolbar.component';
import {CommonModule} from '@angular/common';
import {ModelsModule} from '../../models/models.module';
import {Placeholder} from '@angular/compiler/src/i18n/i18n_ast';
import {StatisticComponent} from './components/statistic/statistic.component';
import {PlannerComponent} from './components/planner/planner.component';
import { PlannerCellComponent } from './components/planner/planner-cell/planner-cell.component';
import { MonthSelectorComponent } from './components/planner/month-selector/month-selector.component';
import { DayScheduleComponent } from './components/day-schedule/day-schedule.component';

@NgModule({
  imports: [
    WorkspaceRoutingModule,
    CommonModule,
    ModelsModule
  ],
  declarations: [
    WorkspaceLayoutComponent,
    WorkspaceToolbarComponent,
    PlannerComponent,
    StatisticComponent,
    PlannerCellComponent,
    MonthSelectorComponent,
    DayScheduleComponent
  ]
})
export class WorkspaceModule { }
