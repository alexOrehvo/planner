import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-planner-cell',
  templateUrl: './planner-cell.component.html',
  styleUrls: ['./planner-cell.component.scss']
})
export class PlannerCellComponent implements OnInit {

  @Input()
  public day: number;

  @Input()
  public numberOfTasks: number;

  @Input()
  public enabled: boolean;

  constructor() { }

  ngOnInit() {
  }
}
