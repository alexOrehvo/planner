import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlannerCellComponent } from './planner-cell.component';

describe('PlannerCellComponent', () => {
  let component: PlannerCellComponent;
  let fixture: ComponentFixture<PlannerCellComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlannerCellComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlannerCellComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
