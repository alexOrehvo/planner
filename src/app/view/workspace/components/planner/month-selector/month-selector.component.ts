import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-month-selector',
  templateUrl: './month-selector.component.html',
  styleUrls: ['./month-selector.component.scss']
})
export class MonthSelectorComponent implements OnInit {

  @Input()
  public currentMonth: Date;

  public hidden = true;

  public months: Array<Month> = [
    {id: 0, name: 'January'},
    {id: 1, name: 'February'},
    {id: 2, name: 'March'},
    {id: 3, name: 'April'},
    {id: 4, name: 'May'},
    {id: 5, name: 'June'},
    {id: 6, name: 'July'},
    {id: 7, name: 'August'},
    {id: 8, name: 'September'},
    {id: 9, name: 'October'},
    {id: 10, name: 'November'},
    {id: 11, name: 'December'}
   ];

  private minYear = 2010;
  private maxYear = 2050;

  constructor() { }

  ngOnInit() {
  }

  public getYears(): Array<number> {
    let years = [];
    for (let i = this.minYear; i <= this.maxYear; i++) {
      years.push(i);
    }
    return years;
  }
}

interface Month {
  id: number;
  name: string;
}
