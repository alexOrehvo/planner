import { Component, OnInit } from '@angular/core';
import {PlannerService} from '../../../../models/plannerCell/planner.service';
import {PlannerCell} from '../../../../models/plannerCell/plannerCell.model';

@Component({
  selector: 'app-planner',
  templateUrl: './planner.component.html',
  styleUrls: ['./planner.component.scss']
})
export class PlannerComponent implements OnInit {
  public weekDays: Array<string> = ['Mon', 'Tue', 'Wen', 'Thu', 'Fri', 'Sat', 'Sun' ];
  public table: Array<Array<PlannerCell>> = [[], [], [], [], [], []];
  public currentMonth: Date;

  constructor(private plannerService: PlannerService) {
    this.currentMonth = new Date();
    this.table = this.plannerService.getPlannerCell();
  }

  ngOnInit() {

  }

  public getPlannerCell() {
    console.log(this.table);
  }

  public isCurrentMonth(date: Date): boolean {
    console.log(this.currentMonth.getMonth() === date.getMonth());
    return this.currentMonth.getMonth() === date.getMonth();
  }
}
