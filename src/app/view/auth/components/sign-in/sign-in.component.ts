import { Component, OnInit } from '@angular/core';
import {AccountService} from '../../../../services/account.service';
import {User} from '../../../../models/user/user.model';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

@Component({
    selector: 'app-sign-in',
    templateUrl: './sign-in.component.html',
    styleUrls: ['./sign-in.component.scss']
})
export class SignInComponent implements OnInit {

    public form: FormGroup;

    constructor(
        private fb: FormBuilder,
        private accountService: AccountService,
        private user: User
    ) { }

    ngOnInit() {
        this.form = this.fb.group({
            email: [ null, [ Validators.required, Validators.email ] ],
            password: [ null, [ Validators.required ] ],
        });
    }

    public submitForm(): void {
        for (const i in this.form.controls) {
            if (this.form.controls[ i ]) {
                this.form.controls[ i ].markAsDirty();
                this.form.controls[ i ].updateValueAndValidity();
            }
        }

       const email = this.form.value['email'];
       const password = this.form.value['password'];

       console.log(email + '  ' + password);
    }
}
