import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './components/header/header.component';
import { HeaderLogoComponent } from './components/header/header-logo/header-logo.component';
import { HeaderToolbarComponent } from './components/header/header-toolbar/header-toolbar.component';
import {MainRoutingModule} from '../../routing/main-routing.module';
import { MainLayoutComponent } from './components/main-layout/main-layout.component';

@NgModule({
  declarations: [
      HeaderComponent,
      HeaderLogoComponent,
      HeaderToolbarComponent,
      MainLayoutComponent
  ],
  imports: [
      CommonModule,
      MainRoutingModule
  ]
})
export class MainModule { }
