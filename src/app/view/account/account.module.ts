import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RegistryComponent } from './components/registry/registry.component';
import {AccountRoutingModule} from '../../routing/account-routing.module';
import {FormsModule} from '@angular/forms';

@NgModule({
  declarations: [
      RegistryComponent
  ],
  imports: [
      CommonModule,
      AccountRoutingModule,
      FormsModule
  ]
})
export class AccountModule { }
