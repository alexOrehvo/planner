import { Component, OnInit } from '@angular/core';
import {AccountService} from '../../../../services/account.service';
import {User} from '../../../../models/user/user.model';

@Component({
    selector: 'app-account-register',
    templateUrl: './registry.component.html',
    styleUrls: ['./registry.component.scss']
})
export class RegistryComponent implements OnInit {

    constructor(
        private accountService: AccountService,
        private user: User
    ) { }

    ngOnInit() {
    }

    public submitForm(): void {
        this.accountService.register(this.user);
    }

}
