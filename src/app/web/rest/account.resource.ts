import {HttpClient} from '@angular/common/http';
import {User} from '../../models/user/user.model';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class AccountResource {

    constructor(
        private http: HttpClient
    ) {}

    public registerAccount(account: User): Observable<User[]> {
        console.log(account);
        return this.http.post<User[]>('http://localhost:8080/api/registry', account);
    }


}
