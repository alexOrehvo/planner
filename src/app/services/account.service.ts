import { Injectable } from '@angular/core';
import {User} from '../models/user/user.model';
import {AccountResource} from '../web/rest/account.resource';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AccountService {

  constructor(
      private accountResource: AccountResource
  ) { }

  public register(user: User) {
      this.accountResource.registerAccount(user).subscribe(u => console.log(u));
  }
}
