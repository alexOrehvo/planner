import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';

import {RegistryComponent} from '../view/account/components/registry/registry.component';

const routes: Routes = [
    {
        path: 'create',
        component: RegistryComponent,
    },
    {
        path: '**',
        redirectTo: 'create'
    }
];

@NgModule({
    imports: [ RouterModule.forChild(routes) ],
    exports: [ RouterModule ]
})
export class AccountRoutingModule { }
