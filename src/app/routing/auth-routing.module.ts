import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';

import {SignInComponent} from '../view/auth/components/sign-in/sign-in.component';

const routes: Routes = [
    {
        path: 'sign-in',
        component: SignInComponent,
    },
    {
        path: '**',
        redirectTo: 'sign-in'
    }
];

@NgModule({
    imports: [ RouterModule.forChild(routes) ],
    exports: [ RouterModule ]
})
export class AuthRoutingModule { }
