import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {WorkspaceLayoutComponent} from '../view/workspace/components/workspace-layout/workspace-layout.component';
import {PlannerComponent} from '../view/workspace/components/planner/planner.component';
import {StatisticComponent} from '../view/workspace/components/statistic/statistic.component';

const routes: Routes = [
    {
        path: '',
        component: WorkspaceLayoutComponent,
        children: [
            {
                path: '**',
                component: PlannerComponent
            },
            {
                path: 'statistic',
                component: StatisticComponent
            }
        ]
    }
];

@NgModule({
    imports: [ RouterModule.forChild(routes) ],
    exports: [ RouterModule ]
})
export class WorkspaceRoutingModule { }
