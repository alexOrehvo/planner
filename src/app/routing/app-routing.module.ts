import { NgModule } from '@angular/core';
import { RouterModule, Routes} from '@angular/router';
import { AppComponent } from '../app.component';

const routes: Routes = [
    {
        path: '',
        component: AppComponent,
        children: [
            {
                path: 'planner',
                loadChildren: '../view/main/main.module#MainModule'
            },
            {
                path: 'account',
                loadChildren: '../view/account/account.module#AccountModule'
            },
            {
                path: 'auth',
                loadChildren: '../view/auth/auth.module#AuthModule'
            },
            {
                path: '',
                redirectTo: 'planner',
                pathMatch: 'full'}
        ]
    }
];

@NgModule({
    imports: [ RouterModule.forRoot(routes) ],
    exports: [ RouterModule ]
})
export class AppRoutingModule { }
