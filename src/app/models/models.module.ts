import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {PlannerService} from './plannerCell/planner.service';
import {User} from './user/user.model';

@NgModule({
    declarations: [],
    imports: [
        CommonModule
    ],
    providers: [
        PlannerService,
        User
    ]
})
export class ModelsModule { }
