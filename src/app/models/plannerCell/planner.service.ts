import { Injectable } from '@angular/core';
import {PlannerCell} from './plannerCell.model';

@Injectable()
export class PlannerService {

  private WEEK_LENGTH = 7;
  private NUMBER_OF_ROW = 6;

  private firstDayOfCurrentMonth: Date;
  private firstDayInTable: Date;
  public cells: Array<Array<PlannerCell>> = [[], [], [], [], [], []];
  private year: number;
  private month: number;
  private day: number;

  constructor() {
    let date = new Date();
    this.year = date.getFullYear();
    this.month = date.getMonth();
    this.firstDayOfCurrentMonth = new Date(this.year, this.month);
    this.day = this.firstDayOfCurrentMonth.getDate();
}

  public getPlannerCell(): Array<Array<PlannerCell>> {
    let firstDay = this.getFirstDateInTable();
    let year = firstDay.getFullYear();
    let month = firstDay.getMonth();
    let day = firstDay.getDate();
    for (let i = 0; i < this.NUMBER_OF_ROW; i++) {
      for (let j = 0; j < this.WEEK_LENGTH; j++) {
        this.cells[i].push(new PlannerCell(new Date(year, month, day + i * this.WEEK_LENGTH + j)));
      }
    }
    return this.cells;
  }

  public getFirstDateInTable(): Date {
    this.firstDayInTable = new Date(this.year, this.month, this.day - this.getDayOfWeek(this.firstDayOfCurrentMonth));
    return this.firstDayInTable;
  }

  public getDayOfWeek(day: Date): number {
    if (day.getDay() === 0) {
      return 6;
    } else {
      return day.getDay() - 1;
    }
  }
}

enum DaysOfWeek {
  Sunday = 0,
  Monday = 1,
  Tuesday = 2,
  Wednesday = 3,
  Thursday = 4,
  Friday = 5,
  Saturday = 6,
}
